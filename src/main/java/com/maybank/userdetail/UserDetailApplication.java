package com.maybank.userdetail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class UserDetailApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserDetailApplication.class, args);
	}
}
