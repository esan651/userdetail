package com.maybank.userdetail.constant;

public class UserConstant {

	public static final String FAILED_CREATE_USER = "Failed to create user ";
	public static final String FAILED_UPDATE_USER = "Failed to update user ";
	public static final String FAILED_FETCH_USER = "Failed fetch user";
	public static final String FAILED_DELETE_USER = "Failed delete user";

	public static final String API_URL = "https://jsonplaceholder.typicode.com/posts";

}
