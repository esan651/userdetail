package com.maybank.userdetail.user;

public class UserDTO {

	private long userId;
	
	private long id;
	
	private String title;
	
	private String body;
	
//	@JsonFormat(pattern = "dd-MM-yyy", timezone = "GMT+8")
//	private Date createdtDate;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

//	public Date getCreatedtDate() {
//		return createdtDate;
//	}
//
//	public void setCreatedtDate(Date createdtDate) {
//		this.createdtDate = createdtDate;
//	}

}
