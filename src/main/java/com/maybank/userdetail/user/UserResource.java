package com.maybank.userdetail.user;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maybank.userdetail.constant.UserConstant;

@RestController
@RequestMapping("/api")
public class UserResource {

	private final Logger log = LoggerFactory.getLogger(UserResource.class);

	@Autowired
	UserService userService;

	@SuppressWarnings("rawtypes")
	@PostMapping("/v1/user")
	public ResponseEntity createUser(@Valid @RequestBody UserDTO userDTO, HttpServletRequest request)
			throws URISyntaxException {
		try {
			log.debug("REST request to create user : {}", userDTO);
			UserDTO newUserDTO = userService.createUser(userDTO);
			return ResponseEntity.created(new URI("/v1/user" + userDTO.getUserId())).body(newUserDTO);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(500).body(UserConstant.FAILED_CREATE_USER);
		}
	}

	@PutMapping("/v1/user")
	public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO, HttpServletRequest request)
			throws URISyntaxException {
		try {
			if (userDTO.getUserId() == 0) {
				return ResponseEntity.badRequest().body("user id was empty");
			} else {
				UserDTO updatedUserDTO = userService.editUser(userDTO);
				return ResponseEntity.ok().body(updatedUserDTO);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(500).body(UserConstant.FAILED_UPDATE_USER);
		}
	}

	@GetMapping("/v1/user")
	public ResponseEntity<?> getAllUser(HttpServletRequest request) throws URISyntaxException {
		try {
			log.debug("REST request to retrieve all users");
			List<UserDTO> userDTOList = userService.getAllUser();
			return ResponseEntity.ok().body(userDTOList);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(500).body(UserConstant.FAILED_FETCH_USER);
		}

	}

	@GetMapping("/v1/user/{id}")
	public ResponseEntity<?> getUserById(@PathVariable String id, HttpServletRequest request)
			throws URISyntaxException {
		try {
			log.debug("REST request to retrieve user by id");
			UserDTO userDTO = userService.getUser(id);
			return ResponseEntity.ok().body(userDTO);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(500).body(UserConstant.FAILED_FETCH_USER);
		}

	}

	@DeleteMapping("/v1/user/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable String id, HttpServletRequest request)
			throws URISyntaxException {
		try {
			log.debug("REST request to delete user by id");
			userService.deleteUser(id);
			return ResponseEntity.ok().body(null);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(500).body(UserConstant.FAILED_DELETE_USER);
		}
	}
}
