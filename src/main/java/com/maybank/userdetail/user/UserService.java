package com.maybank.userdetail.user;

import java.net.URISyntaxException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service

public class UserService {

	@Autowired
	private WebClient webClient;

	private static final String BASE_URL = "https://jsonplaceholder.typicode.com/posts";

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	// sample using rest template
	public List<UserDTO> getAllUser() throws URISyntaxException {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<UserDTO>> response = restTemplate.exchange(BASE_URL, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UserDTO>>() {
				});
		List<UserDTO> userList = response.getBody();
		return userList;
	}

	// sample using webclient
	public UserDTO getUser(String id) {
		return webClient.get().uri(String.join("", BASE_URL + "/", id)).retrieve()
				.onStatus(HttpStatus::is4xxClientError, error -> Mono.error(new RuntimeException("API not found")))
				.onStatus(HttpStatus::is5xxServerError,
						error -> Mono.error(new RuntimeException("Server is not responding")))
				.bodyToMono(UserDTO.class).block();
	}

	public UserDTO createUser(UserDTO userDTO) {
		return webClient.post().uri(BASE_URL).body(Mono.just(userDTO), UserDTO.class).retrieve()
				.onStatus(HttpStatus::is4xxClientError, error -> Mono.error(new RuntimeException("API not found")))
				.onStatus(HttpStatus::is5xxServerError,
						error -> Mono.error(new RuntimeException("Server is not responding")))
				.bodyToMono(UserDTO.class).block();

	}

	public UserDTO editUser(UserDTO userDTO) {
		return webClient.put().uri(String.join("", BASE_URL + "/", String.valueOf(userDTO.getId())))
				.body(Mono.just(userDTO), UserDTO.class).retrieve()
				.onStatus(HttpStatus::is4xxClientError, error -> Mono.error(new RuntimeException("API not found")))
				.onStatus(HttpStatus::is5xxServerError,
						error -> Mono.error(new RuntimeException("Server is not responding")))
				.bodyToMono(UserDTO.class).block();

	}

	public Void deleteUser(String id) {
		return webClient.delete().uri(String.join("", BASE_URL + "/", id)).retrieve()
				.onStatus(HttpStatus::is4xxClientError, error -> Mono.error(new RuntimeException("API not found")))
				.onStatus(HttpStatus::is5xxServerError,
						error -> Mono.error(new RuntimeException("Server is not responding")))
				.bodyToMono(Void.class).block();
	}

}
